/**
 * Author:Akombo Neville Akwo
 */

/**
 * The main.js is your application main.js
 * import the functions in config.js
 */
import {loadFacebookSDK,loadGoogleClient} from "./config/config";

/**
 * Call the functions before app creation
 */

loadGoogleClient(document, "script", "google-client");
loadFacebookSDK(document, "script", "facebook-jssdk");

/**
 * Create your app below
 */