# Google and Facebook social login for vuejs

A simple implementation of social login with facebook and sign in with google using vue js

## Getting started

- [ ] Pull the project
- [ ] Copy the code in main.js to your main.js
- [ ] Copy the config folder to your project
- [ ] Add FacebookLogin.vue and GoogleSign.vue in components folder
- [ ] Boom there you


## Name
Facebook and Google Social Login with Vuejs

## Author
Akombo Neville Akwo

## Tags
Facebook ,Vuejs,Google, Social Login

## License
For open source projects, say how it is licensed.
