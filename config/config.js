/**
 *
 * @param d document
 * @param s dom element
 * @param id id of the element
 */
export function loadGoogleClient(d, s, id) {
    let js,
        fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://accounts.google.com/gsi/client";
    fjs.parentNode.insertBefore(js, fjs);
}

/**
 *
 * @param d document
 * @param s dom element
 * @param id id of the element
 */
export function loadFacebookSDK(d, s, id) {
    let js,
        fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}